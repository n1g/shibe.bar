#!/bin/bash
set -ex
echo "SharX GitLab migration script v1.0"
#if [ "$EUID" -ne 0 ]; then
#  echo "Please run as root or by prefixing the command with 'sudo'!"
#  exit
#fi

cd "$(dirname "$0")" || exit

BACKUP_TAR=$(mktemp "${TMPDIR:-/tmp/}SharX_backup.XXXXXXXXXXXX.tar.gz")

echo "Creating backup tar archive, this may take some time..."
tar -czvf "$BACKUP_TAR" ./images/ ./config/

echo "Your changes to all the files will be lost and the repository will be re-cloned!"
echo "(Only the 'config' and 'images' folder will persist)"
echo "To cancel, CTRL-C in the next 5 seconds!"
sleep 5

echo "Proceeding"
DIRNAME="$(basename "$(pwd)")"
cd ..
rm -rf "./$DIRNAME/"

git clone https://gitlab.com/GGORG/SharX.git "$DIRNAME"

cd "$DIRNAME" || exit

tar -C . -xzvf "$BACKUP_TAR"

pip install -r requirements.txt || python3 -m pip install requirements.txt || python -m pip install requirements.txt || echo " => Could not install requirements.txt!"

echo "Migration successfully finished!"
echo "In some WSGI servers, you will need to restart the app now."

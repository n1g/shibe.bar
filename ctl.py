#!/usr/bin/env python3
import os
import secrets
import signal
import subprocess

import argh
from tabulate import tabulate

from main import query_db, get_db, config, app


@argh.arg("--host", "-h", help="Specify the IP/host to listen on")
@argh.arg("--port", "-p", help="Specify the port to listen on")
@argh.arg("--debug", "-d", help="This enables the Flask debug mode")
def server(host="0.0.0.0", port=5555, debug=False):
    """Starts a Flask development testing server locally"""
    print("Starting development testing server...")
    app.run(host=host, port=port, debug=debug)


@argh.arg("--pid", "-p", help="Specify a PID manually, otherwise it will be pulled from pid.txt")
def restart_wsgi(pid=0):
    """Restarts the WSGI server by killing the process"""
    if pid != 0:
        os.kill(pid, signal.SIGINT)
        return
    with open("pid.txt", "r") as f:
        pids = f.readlines()
    for process in pids:
        process = process.strip()
        if process == "":
            continue
        try:
            os.kill(int(process), signal.SIGINT)
            print("Successfully killed process " + process)
        except ProcessLookupError:
            print("Failed to kill process " + process)
    with open("pid.txt", "w") as f:
        f.write("")


def update():
    """Updates the Git repo and restarts the WSGI server"""
    proc = subprocess.Popen(["git", "pull"])
    proc.wait()
    proc = subprocess.Popen(["pip", "install", "-r", "requirements.txt"])
    proc.wait()
    restart_wsgi()


def users_list():
    with app.app_context():
        users = query_db("SELECT * FROM users")
    new_users = []
    for user in users:
        new_users.append(list(dict(user).values()))
    return tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls']] + new_users,
                    headers='firstrow', tablefmt='fancy_grid')


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
def users_del(uid=-1, username=""):
    with app.app_context():
        if uid != -1:
            user = query_db('SELECT * FROM users WHERE uid = ?',
                            [uid], one=True)
            username = user['username']
        elif username != "":
            user = query_db('SELECT * FROM users WHERE username = ?',
                            [username], one=True)
            uid = user['uid']
        else:
            return "Please provide either a username or a UID!"

    print(f"Deleting user {username}: ")
    print(tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                    list(dict(user).values())], headers='firstrow', tablefmt='fancy_grid'))
    proceed = input("Do you want to proceed? [Y/n]: ")
    if proceed.lower() in ["", "y"]:
        with app.app_context():
            usr_images = query_db("SELECT * FROM images WHERE user = ?", [uid])

        for img in usr_images:
            file = img['id'] + img['ext']
            os.remove(os.path.join(config['storage_folder'], file))

        with app.app_context():
            db = get_db()
            db.cursor().execute("DELETE FROM users WHERE uid = ?", [uid])
            db.commit()

        return "User deleted successfully!"
    else:
        return "Cancelled"


def users_set(uid: int, key: str, value: str):
    with app.app_context():
        user = query_db('SELECT * FROM users WHERE uid = ?',
                        [uid], one=True)

    print("Before:")
    print(tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                    list(dict(user).values())], headers='firstrow', tablefmt='fancy_grid'))
    print("After:")
    user_after = dict(user)
    if isinstance(user[key], int):
        value: int = int(value)
    user_after[key] = value
    print(tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                    list(dict(user_after).values())], headers='firstrow', tablefmt='fancy_grid'))
    proceed = input("Do you want to proceed? [Y/n]: ")
    if proceed.lower() in ["", "y"]:
        with app.app_context():
            db = get_db()
            db.cursor().execute(
                f"UPDATE users SET \"{key}\" = ? WHERE uid = ?",
                [value, uid])
            db.commit()

        return "Property changed successfully"
    else:
        return "Cancelled"


def users_getuid(username: str):
    with app.app_context():
        user = query_db('SELECT * FROM users WHERE username = ?',
                        [username], one=True)
    uid = user['uid']

    return f"User with username {username} has UID {uid}"


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
def users_get(uid=-1, username=""):
    with app.app_context():
        if uid != -1:
            user = query_db('SELECT * FROM users WHERE uid = ?',
                            [uid], one=True)
            return tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                             list(dict(user).values())], headers='firstrow', tablefmt='fancy_grid')
        elif username != "":
            user = query_db('SELECT * FROM users WHERE username = ?',
                            [username], one=True)
            return tabulate([['uid', 'username', 'email', 'password_hash', 'key', 'storage_used', 'invisibleurls'],
                             list(dict(user).values())], headers='firstrow', tablefmt='fancy_grid')
        else:
            return "Please provide either a username or a UID!"


def stats():
    with app.app_context():
        img_count = query_db("SELECT count(*) FROM images", one=True)[0]
        usr_count = query_db("SELECT count(*) FROM users", one=True)[0]
        users = query_db("SELECT * FROM users")
        total_storage = round(sum(user['storage_used'] for user in users) / (1024 * 1024), 2)
    return tabulate(
        {"Image count": [img_count], "User count": [usr_count], "Storage used": [str(total_storage) + " MB"]},
        headers='keys', tablefmt='fancy_grid')


def imgs_del(id: str):
    with app.app_context():
        img = query_db('SELECT * FROM images WHERE id = ?',
                       [id], one=True)

    print(f"Deleting image {id}: ")
    print(tabulate([['id', 'name', 'ext', 'upload_time', 'size_b', 'user'],
                    list(dict(img).values())], headers='firstrow', tablefmt='fancy_grid'))
    proceed = input("Do you want to proceed? [Y/n]: ")
    if proceed.lower() in ["", "y"]:
        with app.app_context():
            user = query_db('SELECT * FROM users WHERE uid = ?',
                            [img['user']], one=True)
            os.remove(os.path.join(config['storage_folder'], str(
                user['uid']), id + img['ext']))
            db = get_db()
            db.cursor().execute(
                "UPDATE users SET storage_used = ? WHERE uid = ?",
                [user['storage_used'] - img['size_b'], user['uid']])
            db.cursor().execute("DELETE FROM images WHERE id = ?", [id])
            db.commit()

        return "Image deleted successfully!"
    else:
        return "Cancelled"


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
def users_img_count(uid=-1, username=""):
    with app.app_context():
        if uid != -1:
            user = query_db('SELECT * FROM users WHERE uid = ?',
                            [uid], one=True)
        elif username != "":
            user = query_db('SELECT * FROM users WHERE username = ?',
                            [username], one=True)
        else:
            return "Please provide either a username or a UID!"
        imgs = query_db('SELECT COUNT(*) FROM images WHERE user = ?',
                        [user['uid']], one=True)
        return str(imgs[0])


@argh.arg("--count", "-c", help="Specifies the amount of best users to return")
def users_best(count=5):
    with app.app_context():
        users = query_db('SELECT * FROM users')
        users_with_img_count = []
        for user in users:
            imgs = query_db('SELECT COUNT(*) FROM images WHERE user = ?',
                            [user['uid']], one=True)
            users_with_img_count.append(
                (user['uid'], user['username'], round(user['storage_used'] / (1024 * 1024), 2), imgs[0]))

        users_with_img_count = list(sorted(users_with_img_count, key=lambda user: user[3]))
        users_top = users_with_img_count[-count:]
        users_top.reverse()
        return tabulate(
            [['uid', 'username', 'storage_used_mb', 'image_count']] + users_top,
            headers='firstrow', tablefmt='fancy_grid')


@argh.arg("--count", "-c", help="Specifies the amount of invites")
def invites_wave(count=1):
    with app.app_context():
        users = query_db('SELECT * FROM users')
        for user in users:
            for _ in range(count):
                code = secrets.token_hex(8)
                db = get_db()
                db.cursor().execute("INSERT INTO invites VALUES (?, ?)",
                                    [
                                        code,
                                        user['uid']
                                    ]
                                    )
                db.commit()
        return f"Invite wave of {count} invite(s) successful!"


@argh.arg("--uid", "-i")
@argh.arg("--username", "-n")
@argh.arg("--count", "-c", help="Specifies the amount of invites")
def invites_gen(uid=-1, username="", count=1):
    with app.app_context():
        if uid != -1:
            user = query_db('SELECT * FROM users WHERE uid = ?',
                            [uid], one=True)
        elif username != "":
            user = query_db('SELECT * FROM users WHERE username = ?',
                            [username], one=True)
        else:
            return "Please provide either a username or a UID!"
        for _ in range(count):
            code = secrets.token_hex(8)
            db = get_db()
            db.cursor().execute("INSERT INTO invites VALUES (?, ?)",
                                [
                                    code,
                                    user['uid']
                                ]
                                )
            db.commit()
        return f"Successfully gave {count} invite(s) to {user['username']} (uid {user['uid']})"


def invites_revoke(code: str):
    with app.app_context():
        inv = query_db('SELECT * FROM invites WHERE code = ?', [code], one=True)
        user = query_db('SELECT * FROM users WHERE uid = ?', [inv['user']], one=True)
        db = get_db()
        db.cursor().execute("DELETE FROM invites WHERE code = ?", [code])
        db.commit()
        return f"Successfully revoked invite {code} from {user['username']} (uid {user['uid']})"


@argh.arg("--table", "-t")
def db_query(sql: str, table: bool = False):
    with app.app_context():
        output = [tuple(row) for row in query_db(sql)]
        if table:
            return tabulate(output, tablefmt='fancy_grid')
        else:
            return output


def db_execute(sql: str):
    with app.app_context():
        db = get_db()
        db.cursor().execute(sql)
        db.commit()
        return "Executed SQL command successfully"


if __name__ == "__main__":
    parser = argh.ArghParser()
    parser.add_commands([server, restart_wsgi, update])
    parser.add_commands(
        [users_list, users_del, users_set, users_getuid, users_get, stats, imgs_del, users_img_count, users_best,
         invites_gen, invites_wave, invites_revoke, db_query, db_execute],
        namespace="ctl")
    parser.dispatch()

# SharX

This is a ShareX uploader server written in Python Flask, also featuring a web frontend and a dashboard.

## Contributing, issues and bugs

This project can be unstable sometimes. If you find any bugs, please report them in the issues tab or make a pull
request with the fix. Chances are that an issue is worked on now, so please watch the commits and wait a while, maybe a
fix will be released. Thanks for all contributions!

You can also join the [Discord server](https://sharx.host/discord) and discuss the issue there or DM me.

## Hosted instance

There is a 24/7, up-to-date hosted by me instance of it available over at https://sharx.host

## Migration

If you previously used the GitHub ShareX-Python version, please back the whole folder up, just in case and run
the `migrate.sh` script.

## Config

1. First, create a `config` directory.
2. There is a `config.json.example` file, copy it as `config/config.json`
3. Fill out all the values in the config:
    - `allowed_extensions`
      As the name suggests, here you should put file extensions, that are allowed.
    - `motd`
      Like most other ShareX hosting services this one also contains a MOTD. It's basically like a description.
    - `storage_folder`
      This is the folder name, that will be used for storing images. Most likely you won't touch this except if you use
      an external drive. Then, use `/mnt/drive` or any other mountpoint or directory.
    - `name`
      This is your host's name. Put whatever you want in there.
    - `discord`
      This is the invite **code** for your Discord server. That means that if my invite is `https://discord.gg/abcdef`,
      the code will be `abcdef`
    - `enable_invites`
      Set this to `true` to enable the invite system. Users will have to enter an invite code to register.
4. Add a favicon as `config/favicon.ico`
5. Add a logo as `config/logo.png`
6. Install the requirements using `pip install -r requirements.txt`
7. Start the host using a WSGI server and enjoy! A startup file for *Phusion Passenger* and other common WSGI servers is
   included by default.

## Note on invisible URLs

Invisible URLs are a bit broken in Flask, because they contain non-standard characters that only exist in the utf-8
codec. When visiting an invisible URL, you'll get a *500 internal server error*. To fix it, look at the error and find
the path with `werkzeug/_internal.py`. If you're using a *virtualenv* aka. *venv*, it will be
in `venv/lib/python3.something/site-packages/werkzeug/_internal.py`. Open it with a text editor (sometimes you'll
need `sudo`) and change the following line (around line 153):

```diff
<     return s.encode("latin1").decode(charset, errors)
---
>     return s.encode("utf-8").decode(charset, errors)
```

After that, restart the app with `python3 main.py restart-wsgi` and invisible URLs should be working!

## Note on invites

To create the first account with invites enabled, you need to run `python3 ctl.py ctl db-execute "INSERT INTO invites VALUES ('aaaaaaaaaaaaaaaa', 0)"` and then use `aaaaaaaaaaaaaaaa` as the invite. You can use any other 16-character string as the invite.

## Command line interface

SharX has a command line interface that lets you start a development Flask server or control the host from the command
line. You can use it by running the `main.py` file with commands as the arguments. Below you can see all the options:

```
$ python3 main.py --help
usage: main.py [-h] {server,restart-wsgi,update,ctl} ...

positional arguments:
  {server,restart-wsgi,update,ctl}
    server              Starts a Flask development testing server locally
    restart-wsgi        Restarts the WSGI server by killing the process
    update              Updates the Git repo and restarts the WSGI server
    ctl

optional arguments:
  -h, --help            show this help message and exit
  
$ python3 main.py ctl --help
usage: main.py ctl [-h] {users-list,users-del,users-set,users-getuid,users-get,stats,imgs-del,users-img-count,users-best,invites-gen,invites-wave} ...

positional arguments:
  {users-list,users-del,users-set,users-getuid,users-get,stats,imgs-del,users-img-count,users-best,invites-gen,invites-wave}
    users-list
    users-del
    users-set
    users-getuid
    users-get
    stats
    imgs-del
    users-img-count
    users-best
    invites-gen
    invites-wave

optional arguments:
  -h, --help            show this help message and exit

```

You can view help for any command and sub-command by adding the `-h` or `--help` parameter.

## TODO

- Domains
- Uploading from the dashboard
- URL Shortener
